package com.alexandro45.musicdecoder;

import java.io.*;

public class Main {

    public static void main(String[] args) {
        String[] str = getAllFiles();
        if(str == null){
            System.err.println("Error!");
            System.exit(1);
        }
        int a = 0;
        for (String s:str){
            try {
                FileInputStream fis = new FileInputStream("dir/" + s);
                FileOutputStream fos = new FileOutputStream("dir/" + a + ".mp3");
                byte[] bytes = enccode(fis);
                fos.write(bytes);
                fos.close();
            } catch (IOException e) {
                System.err.println("Error!");
                System.exit(1);
            }
            a++;
        }
    }

    private static byte[] enccode(InputStream in) {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try {

            byte[] bytes = new byte[1024];
            int length = 0;
            int i = 0;
            while ((length = in.read(bytes)) > 0){
                int j = 0;
                while (j < length){
                    bytes[j] = (byte) (bytes[j]^i*17+13&0xff);
                    j++;
                    i++;
                }
                bos.write(bytes);
            }
            bos.close();
            in.close();
        } catch (IOException e) {
            System.err.println("Error!");
            System.exit(1);
        }
        return bos.toByteArray();
    }

    private static String[] getAllFiles(){
        return new File("dir").list();
    }
}
